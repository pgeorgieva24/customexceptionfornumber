﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptionForNumber
{
    public class PositiveNumberException : Exception
    {
        public PositiveNumberException(string message):base(message)
        {
        }
    }

    public class NegativeNumberException : Exception
    {
        public NegativeNumberException(string message) : base(message)
        {
        }
    }

    public class NullNumberException : Exception
    {
        public NullNumberException(string message):base(message)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int? number = null;
            Console.WriteLine("Enter number: ");
            int EnteredNumber;
            bool isnumeric = Int32.TryParse(Console.ReadLine(), out EnteredNumber);

            if (isnumeric)
            {                
                if (EnteredNumber == 0)
                {
                    number = 1;
                }
                else if (EnteredNumber > 0)
                {
                    number = 2;
                }
                else if (EnteredNumber < 0)
                {
                    number = 3;
                }
            }
            


            switch (number)
            {
                case null:
                    throw new NullReferenceException("No assigned number");
                case 1:
                   throw new NullNumberException("The number is 0.");                    
                case 2:
                    throw new PositiveNumberException("The number is positive.");                   
                case 3:
                    throw new NegativeNumberException("The number is negative.");
                
            }
        }
    }
}
